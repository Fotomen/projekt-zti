﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure
{
    public partial class HasloStartoweForm : Form
    {
        //Dozwolone sa trzy proby logawania kolejne 3 sa mozliwe po 15 minutach

        public HasloStartoweForm(string pin)
        {
            InitializeComponent();
            liczbaProbLogowania = 1;
            PIN = pin;
            kolejneMozliweLogowanie = new DateTime(1999, 1, 1);
        }

        private string PIN = null;

        private int liczbaProbLogowania { get; set; }

        private DateTime kolejneMozliweLogowanie { get; set; }

        private const int maksymalnaLiczbaProbLogowania = 3;


        #region Uniemozliwienie zamkniecia okna
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            string haslo = textBox1.Text;
            if (haslo == "")
            {
                MessageBox.Show("Proszę podać PIN."
                    , "Informacja",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (liczbaProbLogowania <= maksymalnaLiczbaProbLogowania || DateTime.Now > kolejneMozliweLogowanie)   
            {
                if (haslo == PIN)     //haslo poprawne zamykamy to okienko
                {
                    Close();
                }
                else
                {
                    int ileZostalo = maksymalnaLiczbaProbLogowania - liczbaProbLogowania;
                    liczbaProbLogowania++;

                    MessageBox.Show("Podałeś błędny PIN. Zostało Tobie "
                        + ileZostalo + " prób logowania."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else    //przekroczono dozwolona liczbe prob logowania na 15 minut
            {
                TimeSpan ts = new TimeSpan(0, 0, 15, 0);    //15minut
                DateTime teraz = DateTime.Now;

                if (kolejneMozliweLogowanie != new DateTime(1999, 1, 1))//tu jest blad
                {
                    kolejneMozliweLogowanie = teraz + ts;
                }

                string kiedy = kolejneMozliweLogowanie.ToShortTimeString() + " " + kolejneMozliweLogowanie.ToShortDateString();


                MessageBox.Show("Przekroczyłeś liczbę możliwych prób logowania."
                    + " Kolejna próba logowania jest możliwa o: " + kiedy
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secure
{
    [Serializable]
    public class GeneratoHaselUstawienia
    {
        public int dlugoscCiagu;
        public bool alfabetLacinski;
        public bool Cyfry;
        public bool znakiSpecjalne;
    }
}

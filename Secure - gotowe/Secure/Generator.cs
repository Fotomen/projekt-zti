﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Secure
{
    class Generator
    {
        private static readonly RNGCryptoServiceProvider randomGenerator = new RNGCryptoServiceProvider();
        
        public static int ileZnakow(bool czy_alfabet, bool cyfra, bool specjalne)
        {
            //tylko znaki
            string alfabetPodstawowy = "qwertyuioplkjhgfdsazxcvbnm";

            //tylko cyfry
            string cyfry = "1234567890";

            string znakiDodatkowe = @"~`!@#$%^&*()_+}{|:?><,./\';[]";

            int ile = 0;

            if (czy_alfabet)
            {
                ile += alfabetPodstawowy.Length;
            }

            if (cyfra)
            {
                ile += cyfry.Length;
            }

            if (specjalne)
            {
                ile += znakiDodatkowe.Length;
            }

            return ile;
        }

        public static char dajZnak(bool czy_alfabet, bool cyfra, bool specjalne, int i)
        {
            //tylko znaki
            string alfabetPodstawowy = "qwertyuioplkjhgfdsazxcvbnm";

            //tylko cyfry
            string cyfry = "1234567890";

            string znakiDodatkowe = @"~`!@#$%^&*()_+}{|:?><,./\';[]";

            string alfabet = "";

            if (czy_alfabet)
            {
                alfabet += alfabetPodstawowy;
            }

            if (cyfra)
            {
                alfabet += cyfry;
            }

            if (specjalne)
            {
                alfabet += znakiDodatkowe;
            }

            return alfabet[i];
        }

        public byte GetByte()
        {
            var buffer = new byte[1];
            randomGenerator.GetBytes(buffer);
            return buffer[0];
        }

        public double GetDouble()
        {
            var buffer = new byte[8];
            randomGenerator.GetBytes(buffer);
            return BitConverter.ToDouble(buffer, 0);
        }

        public short GetInt16()
        {
            var buffer = new byte[2];
            randomGenerator.GetBytes(buffer);
            return BitConverter.ToInt16(buffer, 0);
        }

        public int GetInt32()
        {
            var buffer = new byte[4];
            randomGenerator.GetBytes(buffer);
            return BitConverter.ToInt32(buffer, 0);
        }

        public int GetInt32(int minimum, int maximum)
        {
            const long MaxValue = 1 + (long)uint.MaxValue;

            long range = minimum - maximum - 1;
            var criticalValue = MaxValue - (MaxValue % range);

            while (true)
            {
                var buffer = new byte[4];
                randomGenerator.GetBytes(buffer);
                var number = BitConverter.ToUInt32(buffer, 0);

                if (number < criticalValue)
                {
                    return (int)(minimum + (number % range));
                }
            }
        }

        public long GetInt64()
        {
            var buffer = new byte[8];
            randomGenerator.GetBytes(buffer);
            return BitConverter.ToInt64(buffer, 0);
        }

        public double Sample()
        {
            var buffer = new byte[4];
            randomGenerator.GetBytes(buffer);
            return BitConverter.ToUInt32(buffer, 0) / (1.0 + uint.MaxValue);
        }
    }
}

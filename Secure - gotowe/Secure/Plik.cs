﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Secure
{
    class Plik
    {
        static public bool zapisz(Dane dane, byte[] cztery, byte[] klucz)
        {
            bool ok = true;
            try
            {
                if (dane != null)
                {
                    byte[] wyjscie = null;
                    long dlugosc = -1;
                    bool good = szyfruj(dane, cztery, klucz, ref wyjscie, ref dlugosc);
                    if (good == false)
                    {
                        return false;
                    }

                    //zapis do pliku
                    byte[] num = BitConverter.GetBytes(dlugosc);
                    long rozmiar = num.Length + wyjscie.Length;
                    byte[] bytes = new byte[rozmiar];
                    int i = 0;
                    for (; i < num.Length; i++)
                    {
                        bytes[i] = num[i];
                    }
                    for (int j = 0; i < rozmiar; i++)
                    {
                        bytes[i] = wyjscie[j];
                        j++;
                    }

                    MemoryStream ms = new MemoryStream(bytes);
                    FileStream file = new FileStream(dane.informacjePodstawowe.sciezkaDoPliku, FileMode.Create, FileAccess.Write);
                    ms.WriteTo(file);
                    file.Close();
                    ms.Close();
                }
                else
                {
                    ok = false;
                }
            }
            catch (Exception ex)
            {
                ok = false;
            }
            return ok;
        }

        static public bool wczytaj(byte[] cztery, byte[] klucz, ref Dane dane, string sciezka)
        {
            bool ok = true;
            try
            {
                //odszyfrowywanie
                //dane wejsciowe
                byte[] data = File.ReadAllBytes(sciezka);

                //ustawianie dlugosci
                byte[] size8 = new byte[sizeof(long)];
                int h = 0;
                for (; h < size8.Length; h++)
                {
                    size8[h] = data[h];
                }

                long dlugosc321 = BitConverter.ToInt64(size8, 0);

                byte[] zaszyfrowaneBytes = new byte[data.Length - sizeof(long)];
                for (int k = 0; h < data.Length; k++)
                {
                    zaszyfrowaneBytes[k] = data[h];
                    h++;
                }

                Dane noweDane = new Dane();
                ok = deszyfruj(cztery, klucz, zaszyfrowaneBytes, dlugosc321, ref noweDane);
                dane = noweDane;
            }
            catch (Exception ex)
            {
                ok = false;
            }
            return ok;
        }

        static private bool szyfruj(Dane dane, byte[] cztery, byte[] klucz, ref byte[] wyjscie, ref long dlugosc)
        {
            bool ok = true;
            try
            {
                IFormatter form = new BinaryFormatter();
                MemoryStream ser = new MemoryStream();
                form.Serialize(ser, (object)dane);

                ser.Position = 0;

                Rijndael alg = Rijndael.Create();
                alg.IV = cztery;
                alg.Key = klucz;

                ser.Position = 0;

                MemoryStream enc = new MemoryStream();
                CryptoStream cw = new CryptoStream(enc, alg.CreateEncryptor(), CryptoStreamMode.Write);
                cw.Write(ser.ToArray(), 0, (int)ser.Length);
                cw.FlushFinalBlock();

                enc.Position = 0;
                wyjscie = enc.ToArray();
                dlugosc = ser.Length;
            }
            catch (Exception)
            {
                ok = false;
            }
            return ok;
        }

        static private bool deszyfruj(byte[] cztery, byte[] klucz, byte[] wejscie, long dlugosc, ref Dane wyjscie)
        {
            bool ok = true;
            try
            {
                byte[] benc = wejscie;
                Rijndael alg2 = Rijndael.Create();
                alg2.IV = cztery;
                alg2.Key = klucz;
                IFormatter form2 = new BinaryFormatter();

                MemoryStream bencr = new MemoryStream(benc);

                CryptoStream cr = new CryptoStream(bencr, alg2.CreateDecryptor(), CryptoStreamMode.Read);

                byte[] buf = new byte[dlugosc];
                cr.Read(buf, 0, (int)dlugosc);

                MemoryStream unenc = new MemoryStream(buf);
                unenc.Position = 0;

                Dane resd = (Dane)form2.Deserialize(unenc);
                wyjscie = resd;
            }
            catch (Exception)
            {
                ok = false;
            }
            return ok;
        }

    }
}

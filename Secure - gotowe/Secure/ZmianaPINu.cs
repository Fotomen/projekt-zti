﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure
{
    public partial class ZmianaPINu : Form
    {
        public string pin;

        public ZmianaPINu(string pin)
        {
            InitializeComponent();
            this.pin = pin;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //sprawdzenie czy textboxy sa puste
            if (textBox1.Text=="")
            {
                MessageBox.Show("Brak aktualnego PINu. Proszę podać PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Text = "";
                textBox1.Focus();
                return;
            }

            if (textBox2.Text == "")
            {
                MessageBox.Show("Proszę podać nowy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Focus();
                return;
            }

            if (textBox3.Text == "")
            {
                MessageBox.Show("Proszę podać nowy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Focus();
                return;
            }

            //sprawdzenie czy oba nowe piny sa takie same
            string nowyPIN = textBox2.Text;
            if (nowyPIN!=textBox3.Text)//wtedy jest blad
            {
                textBox2.Text = textBox3.Text = "";
                textBox2.Focus();

                MessageBox.Show("Podane PINy różnią się. Proszę podać nowy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //sprawdzenie czy nowy pin sklada sie z samych cyfr
            bool sameCyfry = true;
            int rozmiar = nowyPIN.Length;
            for (int i = 0; i < rozmiar; i++)
            {
                if ( Char.IsNumber(nowyPIN[0]) == false)
                {
                    sameCyfry = false;
                    break; //koniec petli
                }
            }
            if (sameCyfry == false)
            {
                textBox2.Text = textBox3.Text = "";
                textBox2.Focus();

                MessageBox.Show("PIN nie składa się z samych cyfr. Proszę podać nowy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //sprawdzenie czy ma dlugosc 4, bo moze miec np. 2
            if (rozmiar != 4)
            {
                textBox2.Text = textBox3.Text = "";
                textBox2.Focus();

                MessageBox.Show("PIN nie ma odpowiedniej długości. Proszę podać nowy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //sprawdzenie czy podano dobry aktualny pin
            if (pin != textBox1.Text)
            {
                MessageBox.Show("Podano błędy PIN."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Focus();
                return;
            }

            //zapis nowego pinu
            pin = nowyPIN;


            MessageBox.Show("PIN został zmieniony."
                    , "Informacja",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure
{
    public partial class HaslaUstawieniaForm : Form
    {
        private GeneratoHaselUstawienia ustawienia;

        public HaslaUstawieniaForm(ref GeneratoHaselUstawienia ustawienia)
        {
            InitializeComponent();
            this.ustawienia = ustawienia;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //zapisz nowe wartosci i zamknij okno
            ustawienia.dlugoscCiagu = (int)numericUpDown1.Value;
            if (checkBox1.Checked==true)
            {
                ustawienia.alfabetLacinski = true;
            }
            else
            {
                ustawienia.alfabetLacinski = false;
            }

            if (checkBox2.Checked == true)
            {
                ustawienia.Cyfry = true;
            }
            else
            {
                ustawienia.Cyfry = false;
            }

            if (checkBox3.Checked == true)
            {
                ustawienia.znakiSpecjalne = true;
            }
            else
            {
                ustawienia.znakiSpecjalne = false;
            }

            MessageBox.Show("Zmieniono ustawienia."
                    , "Informacja",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

            Close();
        }

        private void HaslaUstawieniaForm_Load(object sender, EventArgs e)//wczytaj aktualne wartosci
        {
            numericUpDown1.Value = ustawienia.dlugoscCiagu;

            if (ustawienia.alfabetLacinski==true)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }

            if (ustawienia.Cyfry == true)
            {
                checkBox2.Checked = true;
            }
            else
            {
                checkBox2.Checked = false;
            }

            if (ustawienia.znakiSpecjalne == true)
            {
                checkBox3.Checked = true;
            }
            else
            {
                checkBox3.Checked = false;
            }
        }
    }
}

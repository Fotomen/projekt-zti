﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure
{
    public partial class GeneratorHasel : Form
    {
        private GeneratoHaselUstawienia ustawienia;

        public GeneratorHasel(ref GeneratoHaselUstawienia ustawienia)
        {
            InitializeComponent();
            this.ustawienia = ustawienia;
        }

        private string ciagPseudolosowy = "";

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false && checkBox2.Checked == false && checkBox3.Checked==false)
            {
                MessageBox.Show("Proszę wybrać alfabet."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                 return;
            }

            label1.Text = "Pracuję...";
            label1.Refresh();

            int ileznakow = Generator.ileZnakow(checkBox1.Checked,
                                            checkBox2.Checked,
                                            checkBox3.Checked);

            int rozmiar = (int) numericUpDown1.Value;

            Generator gen=new Generator();

            char[] slowo = new char[rozmiar];

            for (int i = 0; i < rozmiar; i++)
            {
                slowo[i] = Generator.dajZnak(checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, gen.GetInt32(0, ileznakow - 1));
            }

            textBox1.Text = new string(slowo);
            label1.Text = "";

            //zapis ustawien
            ustawienia.dlugoscCiagu = (int)numericUpDown1.Value;
            if (checkBox1.Checked == true)
            {
                ustawienia.alfabetLacinski = true;
            }
            else
            {
                ustawienia.alfabetLacinski = false;
            }

            if (checkBox2.Checked == true)
            {
                ustawienia.Cyfry = true;
            }
            else
            {
                ustawienia.Cyfry = false;
            }

            if (checkBox3.Checked == true)
            {
                ustawienia.znakiSpecjalne = true;
            }
            else
            {
                ustawienia.znakiSpecjalne = false;
            }
            
            //wynik
            ciagPseudolosowy = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(ciagPseudolosowy);
        }

        private void GeneratorHasel_Load(object sender, EventArgs e)
        {
            //wczytanie ustawien
            numericUpDown1.Value = ustawienia.dlugoscCiagu;

            if (ustawienia.alfabetLacinski == true)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }

            if (ustawienia.Cyfry == true)
            {
                checkBox2.Checked = true;
            }
            else
            {
                checkBox2.Checked = false;
            }

            if (ustawienia.znakiSpecjalne == true)
            {
                checkBox3.Checked = true;
            }
            else
            {
                checkBox3.Checked = false;
            }
        }
    }
}

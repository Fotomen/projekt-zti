﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secure
{
    [Serializable]
    struct Info
    {
        public GeneratoHaselUstawienia ustawieniaGeneratoraHasel;
        public string pin { get; set; }
        public string sciezkaDoPliku { get; set; }
    }

    [Serializable]
    public struct Element
    {
        public DateTime WaznoscOd;
        public string Kategoria { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
        public string Email { get; set; }
        public string nazwaSerwisu { get; set; }
        public string linkWWW { get; set; }
        public Serwis Serwis { get; set; }
        public bool Pokaz { get; set; }
    }

    [Serializable]
    public enum Serwis { brak, gmail , outlook }

    [Serializable]
    class Dane
    {
        public Info informacjePodstawowe = new Info();
        public List<Element> daneLogowania = new List<Element>();
    }
}

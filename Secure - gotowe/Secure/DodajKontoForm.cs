﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure
{
    public partial class DodajKontoForm : Form
    {
        public Element noweKonto = new Element();
        public bool dodanoKonto = false;
        public List<string> maile = null;
        public List<string> kategorie = null;
        private bool czyModyfikacja = false;
        private Element modyfikowany;
        public bool zapisanoModyfikacje = false;

        public DodajKontoForm()
        {
            InitializeComponent();

            this.Text = "Dodaj konto";
        }

        public DodajKontoForm(Element wybrany)
        {
            InitializeComponent();
            czyModyfikacja = true;
            this.Text = "Modyfikuj konto";
            modyfikowany = wybrany;

            //przypisywanie wartości
            comboBox1.Text = wybrany.Kategoria;
            textBox5.Text = wybrany.nazwaSerwisu;
            textBox1.Text = wybrany.Login;
            textBox2.Text = wybrany.Haslo;
            textBox3.Text = wybrany.linkWWW;
            comboBox2.Text = wybrany.Email;
            //dodawanie autologowania jest w load
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.UseSystemPasswordChar = false;
            }
            else
            {
                textBox2.UseSystemPasswordChar = true;
            }
        }

        private void DodajKontoForm_Load(object sender, EventArgs e)
        {
            //Dodowanie dostepnych kategorii
            if (kategorie != null)
            {
                foreach (var item in kategorie)
                {
                    comboBox1.Items.Add(item);
                }
            }

            //Dodawanie znanych kont email
            if (maile != null)
            {
                foreach (var item in maile)
                {
                    comboBox2.Items.Add(item);
                }
            }

            //Automatyczne logowanie
            comboBox3.Items.Add("Brak opcji");
            comboBox3.Items.Add("www.gmail.com");
            comboBox3.Items.Add("www.outlook.com");
            
            if (modyfikowany.Serwis==Serwis.brak)
            {
                comboBox3.SelectedIndex = 0;
            }
            else if (modyfikowany.Serwis == Serwis.gmail)
            {
                comboBox3.SelectedIndex = 1;
            }
            else if (modyfikowany.Serwis == Serwis.outlook)
            {
                comboBox3.SelectedIndex = 2;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (czyModyfikacja == false)//dodawanie nowego elementu
            {
                if (comboBox1.Text == "") //kategoria
                {
                    MessageBox.Show("Proszę podać kategorię."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox1.Focus();
                    return;
                }
                if (textBox1.Text == "")  //login
                {
                    MessageBox.Show("Proszę podać login."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                    return;
                }
                if (textBox2.Text == "")  //haslo
                {
                    MessageBox.Show("Proszę podać hasło."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox2.Focus();
                    return;
                }
                if (comboBox2.Text == "") //email
                {
                    MessageBox.Show("Proszę podać adres e-mail."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox2.Focus();
                    return;
                }
                if (textBox5.Text == "")  //nazwa serwisu/konta
                {
                    MessageBox.Show("Proszę podać nazwę konta."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox5.Focus();
                    return;
                }
                if (textBox3.Text == "")  //linkWWW
                {
                    MessageBox.Show("Proszę podać link do serwisu."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox3.Focus();
                    return;
                }
                if (comboBox3.Text == "") //autologowanie
                {
                    MessageBox.Show("Proszę wybrać odpowiednią opcję."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox3.Focus();
                    return;
                }

                noweKonto.Kategoria = comboBox1.Text;
                noweKonto.Login = textBox1.Text;
                noweKonto.Haslo = textBox2.Text;
                noweKonto.Email = comboBox2.Text;
                noweKonto.nazwaSerwisu = textBox5.Text;
                noweKonto.linkWWW = textBox3.Text;

                noweKonto.WaznoscOd = DateTime.Now;
                noweKonto.Pokaz = true;

                Serwis nowySerwis;
                string text = comboBox3.Text;
                if (text == "Brak opcji")
                {
                    nowySerwis = Serwis.brak;
                }
                else if (text == "www.gmail.com")
                {
                    nowySerwis = Serwis.gmail;
                }
                else if (text == "www.outlook.com")
                {
                    nowySerwis = Serwis.outlook;
                }
                else
                {
                    throw new Exception("Nie ma takiej opcji 321!!");
                }

                noweKonto.Serwis = nowySerwis;
                dodanoKonto = true;

                Close();
            }//if
            else
            {
                //modyfikacja

                if (comboBox1.Text == "") //kategoria
                {
                    MessageBox.Show("Proszę podać kategorię."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox1.Focus();
                    return;
                }
                if (textBox1.Text == "")  //login
                {
                    MessageBox.Show("Proszę podać login."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                    return;
                }
                if (textBox2.Text == "")  //haslo
                {
                    MessageBox.Show("Proszę podać hasło."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox2.Focus();
                    return;
                }
                if (comboBox2.Text == "") //email
                {
                    MessageBox.Show("Proszę podać adres e-mail."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox2.Focus();
                    return;
                }
                if (textBox5.Text == "")  //nazwa serwisu/konta
                {
                    MessageBox.Show("Proszę podać nazwę konta."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox5.Focus();
                    return;
                }
                if (textBox3.Text == "")  //linkWWW
                {
                    MessageBox.Show("Proszę podać link do serwisu."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox3.Focus();
                    return;
                }
                if (comboBox3.Text == "") //autologowanie
                {
                    MessageBox.Show("Proszę wybrać odpowiednią opcję."
                        , "Błąd",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox3.Focus();
                    return;
                }

                noweKonto.Kategoria = comboBox1.Text;
                noweKonto.Login = textBox1.Text;
                noweKonto.Haslo = textBox2.Text;
                noweKonto.Email = comboBox2.Text;
                noweKonto.nazwaSerwisu = textBox5.Text;
                noweKonto.linkWWW = textBox3.Text;

                noweKonto.WaznoscOd = DateTime.Now;
                noweKonto.Pokaz = true;

                Serwis nowySerwis;
                string text = comboBox3.Text;
                if (text == "Brak opcji")
                {
                    nowySerwis = Serwis.brak;
                }
                else if (text == "www.gmail.com")
                {
                    nowySerwis = Serwis.gmail;
                }
                else if (text == "www.outlook.com")
                {
                    nowySerwis = Serwis.outlook;
                }
                else
                {
                    throw new Exception("Nie ma takiej opcji 321!!");
                }

                noweKonto.Serwis = nowySerwis;
                zapisanoModyfikacje = true;

                Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

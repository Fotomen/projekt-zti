﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace Secure
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string sciezkaDoPlikuZHaslami = @"C:\file.EPIF";
        byte[] cztery = new byte[] { 217, 209, 131, 181, 185, 62, 58, 210, 176, 218, 3, 155, 109, 153, 70, 9 };
        byte[] klucz = new byte[] { 85, 148, 87, 48, 29, 72, 62, 68, 254, 64, 8, 191, 52, 120, 83, 183, 209, 164, 161, 88, 165, 189, 85, 231, 207, 133, 238, 184, 101, 202, 202, 250 };
        private Dane programData = null;

        //zmienna potrzebna do wyswietlania
        private int poziom = 0;//poziom moze byc 0 lub 1
        private Element wybrany = new Element();//aktualnie wybrane konto z danego serwisu
        private bool wybranoSerwis = false;
        private string powrot = "...";
        private string nazwaKategorii = "";
        private List<Thread> watki=new List<Thread>(); 

        private void Form1_Load(object sender, EventArgs e) //ladowanie sie programu
        {
            if (File.Exists(sciezkaDoPlikuZHaslami) == true)    //plik istnieje trzeba do wczytac i odszyfrowac
            {
                bool ok = Plik.wczytaj(cztery, klucz, ref programData, sciezkaDoPlikuZHaslami);

                if (ok) //udalo sie wczytac plik
                {
                    HasloStartoweForm pinForm = new HasloStartoweForm(programData.informacjePodstawowe.pin);
                    pinForm.ShowDialog();

                    poziom = 0;
                    wyswietlDane();
                }
                else
                {
                    MessageBox.Show("Nie udało się wczytać pliku z hasłami."
                    , "Informacja",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)  //dodawanie nowego konta
        {
            //lista wszystkch maili
            List<string> maile = programData.daneLogowania
                .Select(ele => ele.Email).Distinct().ToList();

            List<string> kategorie = programData.daneLogowania.Select(ele => ele.Kategoria).Distinct().ToList();

            DodajKontoForm form = new DodajKontoForm();
            form.kategorie = kategorie;
            form.maile = maile;
            form.ShowDialog();

            if (form.dodanoKonto == true)
            {
                programData.daneLogowania.Add(form.noweKonto);
            }
            wyswietlDane();
        }

        private void generujHasłoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GeneratorHasel form = new GeneratorHasel(ref programData.informacjePodstawowe.ustawieniaGeneratoraHasel);
            form.ShowDialog();
        }

        private void ustawieniaDomyśToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //zmiana ustawien domyslnych hasel
            HaslaUstawieniaForm form = new HaslaUstawieniaForm(ref programData.informacjePodstawowe.ustawieniaGeneratoraHasel);
            form.ShowDialog();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) 
        {
            foreach (var item in watki)
            {
                item.Abort();
            }

            //tutaj zapis do pliku ustawien
            bool ok = Plik.zapisz(programData, cztery, klucz);
            if (ok == false)
            {
                MessageBox.Show("Nie udało się zapisać danych do pliku."
                    , "Błąd",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void zmieńHasłoRootToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //zmianapinu
            ZmianaPINu form = new ZmianaPINu(programData.informacjePodstawowe.pin);
            form.ShowDialog();

            programData.informacjePodstawowe.pin = form.pin;
        }

        private void zmieńLokalizacjęHasełToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Encrypted Files (EPIF)|*.EPIF";

            DialogResult wynik = dialog.ShowDialog();
            if (wynik == DialogResult.OK)
            {
                sciezkaDoPlikuZHaslami = dialog.FileName;
            }
            else //nie zmieniono pliku
            {
                //nic nie robie
            }
        }

        private void wyswietlDane(string nazwaElementu = "")
        {
            if (poziom == 0) //wyswietlanie kategorii
            {
                //ukryj przyciski
                wybranoSerwis = false;
                pokazPrzyciski(false);

                List<string> nazwyKategorii = new List<string>();

                //skrocony opis:
                //1.znajdz wszystkie elementy ktore sa najstarsze 
                //2.sprawdz czy maja Pokaz == true 
                //3.wybierz z nich kategorie i dodaj do listy

                //znajdz wszystkie konta tj. nazwySerwisow i kategorie(bo w dwoch kategoriach moze byc ta sama nazwa serwisu)
                var nazwySerwisowiKategorie = programData.daneLogowania
                    .Select(ele => new { ele.nazwaSerwisu, ele.Kategoria })
                    .Distinct()
                    .ToList();

                //dla kazdej nazwy serwisu znajdz najstarszy element
                foreach (var nazwy in nazwySerwisowiKategorie)
                {
                    var najstarsszyElement = programData.daneLogowania
                        .Where(ele => ele.nazwaSerwisu == nazwy.nazwaSerwisu && ele.Kategoria == nazwy.Kategoria)
                        .OrderByDescending(ele => ele.WaznoscOd)
                        .FirstOrDefault();

                    //sprawdz czy najstarszy element ma pokaz==true
                    //jesli tak to jego kategorie dodaj do listy kategorii 
                    //jesli w tej liscie nie ma juz takiej kategorii
                    if (najstarsszyElement.Pokaz == true)
                    {
                        //sprawdz czy dana kategoria juz istnieje
                        int rozmiar = nazwyKategorii
                            .Where(ele => ele == najstarsszyElement.Kategoria)
                            .ToList()
                            .Count;

                        if (rozmiar == 0)
                        {
                            nazwyKategorii.Add(najstarsszyElement.Kategoria);
                        }
                    }
                }

                //wczytanie kategorii do listboxa
                listBox1.Items.Clear();
                foreach (var item in nazwyKategorii)
                {
                    listBox1.Items.Add(item);
                }
            }
            else if (poziom == 1)
            {
                pokazPrzyciski(true);

                List<Element> hasla = new List<Element>();

                //tu operujemy na wybranej kategorii(nazwaKategorii)
                var info = programData.daneLogowania.Where(ele => ele.Kategoria == nazwaKategorii).ToList();

                var nazwySerwisow = info
                    .Select(ele => ele.nazwaSerwisu)
                    .Distinct()
                    .ToList();

                foreach (var item in nazwySerwisow)
                {
                    //wybiera najnowsze haslo
                    Element najnowszeHaslo = info
                        .Where(ele => ele.nazwaSerwisu == item)
                        .OrderByDescending(ele => ele.WaznoscOd)
                        .FirstOrDefault();

                    if (najnowszeHaslo.Pokaz != false) //dodajemy tylko elementy nie usuniete
                    {
                        hasla.Add(najnowszeHaslo);
                    }
                }

                listBox1.Items.Clear();
                listBox1.Items.Add(powrot);
                foreach (var item in hasla)
                {
                    listBox1.Items.Add(item.nazwaSerwisu);
                    if (nazwaElementu == item.nazwaSerwisu)
                    {
                        wybrany = item;
                        wybranoSerwis = true;
                        if (listBox1.SelectedIndex != -1)
                        {
                            listBox1.SetSelected(listBox1.SelectedIndex, true);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("zly poziom " + poziom);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (poziom == 0)
            {
                if (listBox1.SelectedItem != null)
                {
                    //ustalenie kategorii
                    poziom++;
                    nazwaKategorii = listBox1.SelectedItem.ToString();
                    wyswietlDane("");
                }
            }
            else if (poziom == 1)
            {
                //jesli pierwszy element to powrot na poziom 0
                //jesli kolejny element to wyswietlenie hasla
                if (listBox1.SelectedItem != null)
                {
                    string wybrano = listBox1.SelectedItem.ToString();
                    if (wybrano == powrot)
                    {
                        poziom--;
                        nazwaKategorii = "";
                        wyswietlDane();
                    }
                    else
                    {
                        wyswietlDane(wybrano);
                    }
                }
            }
            else
            {
                throw new Exception("zly poziom 34 " + poziom);
            }
        }

        private void button2_Click(object sender, EventArgs e)  //login
        {
            if (wybranoSerwis)
            {
                Clipboard.SetText(wybrany.Login);
            }
        }

        private void button3_Click(object sender, EventArgs e)  //haslo
        {
            if (wybranoSerwis)
            {
                Clipboard.SetText(wybrany.Haslo);
            }
        }

        private void button4_Click(object sender, EventArgs e)  //email
        {
            if (wybranoSerwis)
            {
                Clipboard.SetText(wybrany.Email);
            }
        }

        private void button5_Click(object sender, EventArgs e)  //link
        {
            if (wybranoSerwis == true && wybrany.linkWWW != null)
            {
                System.Diagnostics.Process.Start(wybrany.linkWWW);
            }
        }

        private void button6_Click(object sender, EventArgs e)  //autologowanie
        {
            if (wybranoSerwis == true && wybrany.Serwis != Serwis.brak)
            {
                Thread t = new Thread(zaloguj);
                watki.Add(t);
                t.IsBackground = true;
                
                t.Start();
            }
            else
            {
                MessageBox.Show("Automatyczne zalogowanie do tego serwisu jest nie możliwe.",
                "Informacja");
            }
        }

        private void pokazPrzyciski(bool pokaz)
        {
            if (pokaz == true)
            {
                button2.Visible = true;
                button3.Visible = true;
                button4.Visible = true;
                button5.Visible = true;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
            }
            else
            {
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
            }
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void informacjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Program został stworzyony na potrzeby projektu z ZTI w 2015 roku.",
                "Informacja o programie");
        }

        private void button7_Click(object sender, EventArgs e)  //usun konto
        {
            if (wybranoSerwis)
            {
                DialogResult wynik = MessageBox.Show("Czy chcesz usunąć konto: " + wybrany.nazwaSerwisu + " ?",
                    "Usuń konto",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (wynik == DialogResult.Yes)    //usuwamy konto
                {
                    //usuwa element z listy, znaczy zmienia jego widocznosc
                    //zeby to osiagnac dodajemy nowy rekord tego samego konta z widocznoscia false

                    wybrany.WaznoscOd = DateTime.Now;
                    wybrany.Pokaz = false;
                    programData.daneLogowania.Add(wybrany);

                    //trzeba sprawdzic czy nie byl to ostatni serwis w kategorii lub
                    //wrocic na poziom 0 
                    poziom = 0;
                    wyswietlDane();
                }
                else //(wynik==DialogResult.No) //nic nie robimy
                {

                }
            }
        }

        private void button8_Click(object sender, EventArgs e)  //zmien dane konta
        {
            if (wybranoSerwis)
            {
                //lista wszystkch maili
                List<string> maile = programData.daneLogowania
                    .Select(ele => ele.Email).Distinct().ToList();

                List<string> kategorie = programData.daneLogowania.Select(ele => ele.Kategoria).Distinct().ToList();

                DodajKontoForm form = new DodajKontoForm(wybrany);
                form.kategorie = kategorie;
                form.maile = maile;
                form.ShowDialog();

                if (form.zapisanoModyfikacje == true)
                {
                    poziom = 1;
                    wybrany.Pokaz = false;//nie pokazuj starego elementu
                    wybranoSerwis = false;//nie mozna pokazywac na stary element
                    programData.daneLogowania.Add(form.noweKonto);
                    wyswietlDane(); //pokaz od nowa wszystko
                }
            }
        }
        
        public void zaloguj()
        {
            Element ele = wybrany;
            if (ele.Serwis == Serwis.brak)
            {
                //nic nie rob
            }
            else if (ele.Serwis == Serwis.gmail)
            {
                try
                {
                    using (var driver = new ChromeDriver())
                    {
                        TimeSpan ts = new TimeSpan(0, 0, 1, 0);
                        driver.Manage().Timeouts().ImplicitlyWait(ts);

                        driver.Navigate().GoToUrl("https://www.gmail.com/");

                        driver.Manage().Window.Maximize();

                        IWebElement query = driver.FindElement(By.Id("Email"));
                        query.SendKeys(ele.Email);    //mail
                        query = driver.FindElement(By.Id("Email"));
                        query.Click();

                        query = driver.FindElement(By.Id("next"));
                        query.Click();

                        query = driver.FindElement(By.Id("Passwd"));
                        query.SendKeys(ele.Haslo);   //haslo
                        query = driver.FindElement(By.Id("Passwd"));
                        query.Click();

                        query = driver.FindElement(By.Id("signIn"));
                        query.Click();

                        string sciezka = formatDateTime() + " .png";
                        driver.GetScreenshot().SaveAsFile(sciezka, ImageFormat.Png);

                        ts = new TimeSpan(0, 1, 0, 0);
                        driver.Manage().Timeouts().ImplicitlyWait(ts);
                        
                        while (true) ;  //kazda funkcja zaloguj jest osobnym watkiem, to zapobiega samozamykaniu sie okna
                    }
                }
                catch (Exception exception)
                {
                }
            }
            else if (ele.Serwis == Serwis.outlook)
            {
                try
                {
                    using (var driver = new ChromeDriver())
                    {
                        TimeSpan ts = new TimeSpan(0, 0, 1, 0);
                        driver.Manage().Timeouts().ImplicitlyWait(ts);

                        driver.Navigate().GoToUrl("https://www.outlook.com");   //to i tak musi byc

                        driver.Manage().Window.Maximize();

                        IWebElement query = driver.FindElement(By.Id("i0116"));
                        query.SendKeys(ele.Email);    //email
                        query = driver.FindElement(By.Id("i0116"));
                        query.Click();

                        query = driver.FindElement(By.Id("i0118"));
                        query.SendKeys(ele.Haslo);  //haslo
                        query = driver.FindElement(By.Id("i0118"));
                        query.Click();

                        query = driver.FindElement(By.Id("idSIButton9"));
                        query.Click();

                        string sciezka = formatDateTime() + " .png";
                        driver.GetScreenshot().SaveAsFile(sciezka, ImageFormat.Png);

                        ts = new TimeSpan(0, 1, 0, 0);
                        driver.Manage().Timeouts().ImplicitlyWait(ts);

                        while (true) ;
                    }
                }
                catch (Exception exception)
                {
                }
            }//else
            else
            {
            }
        }//zaloguj()

        string formatDateTime()
        {
            DateTime now = DateTime.Now;

            string sciezka = now.ToString("d-m-yyyy HH-mm-ss");

            return sciezka;
        }
    }
}
